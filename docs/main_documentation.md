# Chronicles of Clasu - Official documentation

### Table of contents

1. Introduction
2. Basic usage
3. Main functionality
4. The map engine
5. Music & Sound effects
6. <TO BE ADDED>

#### Introduction

The Chronicles of Clasu is a proposed series of videogames created by the 3rd-year students of the Classical High School of Tampere. The original idea and game were mostly created by Lari Liuhamo in 2016-2017, with the goal of encouraging their teachers to continue teaching programming to the future students.

The game is a role-playing game about some of these teachers getting stuck in an alternative dimension, trying to get out while lending a hand to the residents of that particular world. The player controls one of these teachers, and over the course of his journey unlocks others as battle party members. The main objective is the same in every iteration - getting the heck out of the world they have gotten themselves into.

#### Basic usage

The game runs on Pygame and Python 3. This is to allow anyone to relatively easily modify the games and use them as inspiration or documentation for future games. In order to run the source code, you need to use Python 3 (latest version recommended) with the Pygame-module installed. (The developers mostly used Python 3.5.1.)

You can get the latest version of Python [here](http://www.python.org/downloads/).

Pygame can be installed by running the following in a shell when Python is installed, regardless of your system: `pip install pygame`. Alternatively, if that doesn't work, pick an appropiate version and download it from [here](http://www.pygame.org/download.shtml).

Of course, most versions of Linux already have both Python 2.7 and 3.x installed.

In order to run the source code, just download the GitHub repository (if you haven't already done so), navigate to the src-folder and run main.py. It acts as the game's launcher, and contains the most critical parts of the program code. The other python files are modules that contain other things; config.py contains program-wide constant variables and settings, while sprites.py contains objects for the game, etc.

#### Main functionality

As previously stated, the game consists of a main script and modules that append to it. Additionally, the bgm-folder contains music and sound effects, the sprites-folder obviously contains all of the sprites used in the project and the maps folder contains the game's maps.

Regarding the sound files, while Pygame recognises MP3-files they aren't recommended due to the fact that their license costs are off the charts. Good alternatives include ogg and wav. Other filetypes likely won't work.

#### The map engine

The game's map files are in plain text - they are translated by the main script into the actual game maps. The files' simple nature helps a mapper to design a map without any particular tools besides a normal text editor. As of now, three symbols are recognised:

* '#' represents a wall,
* '.' represents a normal floor tile and
* 'P' represents the player's spawn point

The main script scans the maps folder for any map files that are called during runtime. An example map file could look like this:

```
##########
#.P......#
########.#
#........#
#.########
#........#
#........#
##########
```
