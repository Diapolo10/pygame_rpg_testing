# Project goals

The project is maintained and developed by a team of Finnish high school students. That means, we are in need of more developers. If you are willing to, you can join us and help reach these goals! (Which we can't do in time without enough developers.)

|      Feature       | Developers |
|:------------------:| ----------:|
| Visual map editor  |      5     |
|  Monitor scaling   |      8     |
| Mod/plugin support |     10     |
|   Optional DLC     |     12     |
| Steam achievements |     15     |
|   Mobile support   |     17     |
|     Cutscenes      |     18     |
|  Better graphics   |     20     |
| Paid Steam version |     25     |
|    Multiplayer     |     30     |
| Online high scores |     35     |
|   Extra content    |     40+    |

Of course, these goals cannot be reached without teamwork. We need artists, programmers, musicians, composers, animators... and people who can write the documentation for the next generation (of students)!
