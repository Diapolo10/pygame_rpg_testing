# Design plan

This document contains the head developer's current plans for the project.
As time goes on, it will be updated when certain goals are met or when an
intriguing new idea pops up.

## Table of contents:

* Story
* Music
* Visuals
* Game mechanics
* Project deadline

### Story

The main story is loosely related to the events of the last two Abiturient videos
made by the students of the Classical High School of Tampere. The game continues almost directly from where the video of 2017 ended, casting one of the teachers as the player character.

The player's main objective is to get back home, but he finds himself from the
world of Abitti; a world that should no longer even exist. As the player progresses in the game,
it will slowly become apparent that this world is one of the original backups of Abitti.
Hoping to escape this strange realm, the player character and their companions
enroll on a quest to defeat the backup, and the resurrected Master Sensor, who
is trying to gain all control over it in order to create a world where nothing
interesting happens.

The catch, however, is that when the player eventually does defeat the Master Sensor,
they gain access to their network of other backups. In order to truly escape,
the player must travel between each and every one of these worlds, all of which
are unique in design, until they defeat the last backup and are granted access to
the server core which contains their only way of getting back. (This approach is
to allow future generations to create sequels!)

### Music

Due to budget, and development time, restrictions, the original project will
use music made by other people until further notice. As of now, it's been
decided that Eric Skiff's chiptune soundtracks will be used due to their
suitable licence agreement and their high quality. (The following list consists
of the filenames found in src/bgm/.)

The music is planned to be used as follows:

* bgm00: Main menu theme
* bgm01: Emotional theme
* bgm02: Stage theme, hidden treasure
* bgm03: Strange events (story-related theme)
* bgm04: Friendly encounter, eg. a new party member (teacher) is found
* bgm05: Cutscene, world map or a flashback
* bgm06: Stage theme, likely mountains or a forest
* bgm07: Miniboss theme, Mecha-Sensor
* bgm08: Normal battle theme (?)
* bgm09: Intro cutscene
* bgm10: Final stage theme ('boss level', probably a dark city)
* bgm11: Possibly a theme for shops/cutscenes (emotional)
* bgm12: Stage theme, forest or night-themed level
* bgm13: Battle theme
* bgm14: Final cutscene theme
* bgm15: Ending theme / Credits roll

### Visuals

Arguably the most problematic area of development; no official consensus has
been achieved on what type of graphics the project should use. The currently
planned, and implemented, texture resolutions are 16x16, 32x32 and 64x64.

The most likely option will be 32x32 due to it being simple yet allowing us to add
just enough detail on the sprites. However, support for the other two, and possibly larger,
sprites will still be included in case a future development group would like to use them.

### Game mechanics

As the game is an RPG, with some resemblance to Undertale, it will consist of 8-directional
movement (WASD/arrow keys), interacting with the environment, a message/chat screen,
a combat system that supports up to 4x4 battles (4 heroes, 4 monsters), semi-random drop tables,
equipment (school-related things, including studybooks or rulers) and a branching storyline.

### Project deadline

The deadline, as of 30.11.2016, is 20th of February. However, if the deadline
cannot be met, the project will simply be announced/revealed at the Abiturient
Expo 2017 as part of the other projects, and will continue to be developed until finished.
Whether only one developer is working on the project remains to be seen, but
the project will not be cancelled even if delayed.
