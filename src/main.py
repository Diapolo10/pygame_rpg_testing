#!python3

### Built-in modules ###
import random
import sys
import os
import glob
from xml.etree import cElementTree as et

### Third-party modules ###
import pygame

### Propietary modules ###
import clasu
from sprites import *
from config import *
from tilemap import *

class Game(object):
    """ The main object that handles all game events """
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption(TITLE)
        self.clock = pygame.time.Clock()
        pygame.key.set_repeat(500, 100)
        self.load_data()

    def load_data(self):
        """ Load data from files relevant to the game """
        game_folder = os.path.dirname(__file__)
        img_folder = os.path.join(game_folder, 'sprites')
        bgm_folder = os.path.join(game_folder, 'bgm')
        map_folder = os.path.join(game_folder, 'maps')
        number_of_maps = len(list(glob.glob(os.path.join(map_folder, 'map*.dat'))))

        #self.map = Map(os.path.join(map_folder, 'map1.dat'))
        #self.maps = [Map(map_file) for map_file in glob.glob(os.path.join(map_folder, 'map*.dat'))]
        self.maps = [NewMap(i) for i in range(number_of_maps)]
        self.map_data_root = et.parse('data.xml').getroot()
        self.map_data = [data for data in self.map_data_root[0]]
        self.current_map = self.maps[int(self.map_data[1].get('id'))]
        self.map = self.current_map
        self.texture_theme = self.map.theme
        #self.map = NewMap(1)

        try:
            self.player_img = [pygame.image.load(img).convert_alpha()
                          for img in glob.glob(os.path.join(img_folder, self.texture_theme, PLAYER_SPRITE))]
        except IndexError:
            self.player_img = [pygame.image.load(img).convert_alpha()
                          for img in glob.glob(os.path.join(img_folder, PLAYER_SPRITE))]
            # The player_img is a list of sprites in the order of the numbering.
            # For example, 'cinnamon{size}_{index}.png' would be the sprite
            # for Cinnamon of the given size (16*16, 32*32 or 64*64) and the index
            # tells you which item on the list it is. Say, 'iikka64_1.png'
            # would be a 64*64 image and it would be the 2nd picture on the list.
            # [List indexes begin from 0.]

        try:
            self.wall_img = [pygame.image.load(img).convert_alpha()
                          for img in glob.glob(os.path.join(img_folder, self.texture_theme, WALL_SPRITE))]
        except IndexError:
            self.wall_img = [pygame.image.load(img).convert_alpha()
                          for img in glob.glob(os.path.join(img_folder, WALL_SPRITE))]

        try:
            self.enemy_img = [pygame.image.load(img).convert_alpha()
                          for img in glob.glob(os.path.join(img_folder, self.texture_theme, ENEMY_SPRITE))]
        except IndexError:
            self.enemy_img = [pygame.image.load(img).convert_alpha()
                          for img in glob.glob(os.path.join(img_folder, ENEMY_SPRITE))]
        print(WALL_SPRITE, ENEMY_SPRITE, PLAYER_SPRITE)
        print(self.wall_img, self.player_img, self.enemy_img)

    def new(self):
        """ Initialise a map """
        self.all_sprites = pygame.sprite.Group()
        self.walls = pygame.sprite.Group()
        self.enemies = pygame.sprite.Group()
        self.texture_theme = self.map.theme
        for row, tiles in enumerate(self.map.data):
            for col,tile in enumerate(tiles):
                if tile == '#':
                    Wall(self, col, row)
                if tile == 'P':
                    self.player = Player(self, col, row)
                if tile == 'E':
                    Enemy(self, col, row)
                if tile == 'X': #TODO: Implement map exits
                    pass
        self.camera = Camera(self.map.width, self.map.height)

    def run(self):
        """ Game loop - set self.running to False to end the game """
        self.running = True
        while self.running:
            self.dt = self.clock.tick(FPS) / 1000
            self.events()
            self.update()
            self.draw()

    def quit(self):
        """ Quit the game """
        pygame.quit()
        sys.exit()

    def update(self):
        """ Update the game state """
        self.all_sprites.update()
        self.camera.update(self.player)

    def draw_grid(self):
        """ [DEBUGGING FEATURE] Draw a grid on the camera """
        for x in range(0, WIDTH, TILESIZE):
            pygame.draw.line(self.screen, GREY_LIGHT, (x, 0), (x, HEIGHT))
        for y in range(0, HEIGHT, TILESIZE):
            pygame.draw.line(self.screen, GREY_LIGHT, (0, y), (WIDTH, y))

    def draw(self):
        """ Draw objects on the screen """
        pygame.display.set_caption("{} @ {:.2f}FPS".format(TITLE, self.clock.get_fps()))
        self.screen.fill(BGCOLOUR)
        #self.draw_grid()
        for sprite in self.all_sprites:
            self.screen.blit(sprite.image, self.camera.apply(sprite))
        pygame.draw.rect(self.screen, WHITE, self.camera.apply(self.player), 2)
        pygame.display.flip()

    def events(self):
        """ Monitor all keyboard/mouse events """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.quit()

    def show_start_screen(self): #TODO: Implement the start menu
        """ Display the start menu """
        pass

    def show_go_screen(self): #TODO: Implement the in-game menu
        """ Display the in-game menu """
        pass

class StageHandler(object):
    def __init__(self, game):
        self.game = game
        self.map_data = game.map_data
        self.current_ID = self.game.map.get('id')

    def change_map(self, map_exit):
        self.map_exit = map_exit # EXIT ID
        self.exit_data = {item.get('id'): [item.get('target_map'), item.get('target_entry')] for item in self.map_data[1][self.map_exit]}
        self.target_exit = self.exit_data[self.map_exit]



def main():
    """ Master function """
    game = Game()
    game.show_start_screen()
    while True:
        game.new()
        game.run()
        game.show_go_screen()

if __name__ == "__main__":
    main()
