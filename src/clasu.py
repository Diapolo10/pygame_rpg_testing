#!python3

# Imports:
import math
import random
import pygame

""" NOTE: NONE of these features are currently
implemented. You can help by migrating this to the main project. """

############################ Character classes ############################

class Teacher(object):
    def __init__(self, name, type, exp=0.0):
        self.name = name
        self.type = type
        self.experience = exp
        self.level = 1
        self.health = 100
        self.alive = True
        self.equipment = {"armour": False,
                          "weapon": False}
        self.base_attack = 1+self.level
        self.total_attack = self.base_attack+self.base_attack*self.equipment["weapon"]
        self.base_defence = (self.level/99-1/99)*100
        self.total_defence = self.base_defence+self.base_defence*self.equipment["armour"]

    def __add__(self, value):
        self.experience += value

    def nextLevel(self):
        exponent = 1.6
        baseXP = 85
        return math.floor(baseXP * (self.level**exponent))

    def levelup(self):
        while True:
            if self.nextLevel() <= self.experience:
                self.level += 1
            else:
                print("EXP required for next level:", int(self.nextLevel()-self.experience))
                break

    def give_xp(self, amount):
        self.experience += amount

    def damage(self, amount):
        self.health -= amount
        if self.health <= 0:
            self.health = 0
            self.alive = False

    def heal(self, amount):
        self.health += amount
        if self.health > 100:
            self.health = 100

###########################################################################

class Enemy(object):
    def __init__(self, name, type, drops=[], level=1, MAX_HEALTH=100):
        self.name = name
        self.type = type
        self.level = level
        self.MAX_HEALTH = MAX_HEALTH
        self.health = self.MAX_HEALTH
        self.alive = True
        self.base_attack = self.level
        self.base_defence = self.level
        self.drop_table = drops

    def damage(self, amount):
        self.health -= amount
        if self.health <= 0:
            self.health = 0
            self.alive = False

    def heal(self, amount):
        self.health += amount
        if self.health > self.MAX_HEALTH:
            self.health = self.MAX_HEALTH

###########################################################################

class Weapon(object):
    def __init__(self, name, damage):
        self.name = name
        self.damage = damage

class Armour(object):
    def __init__(self, name, protection):
        self.name = name
        self.protection = protection

###########################################################################

class Battle(object):
    def __init__(self, background, player_party=[], enemy_party=[], bgm=None):
        self.player_party_leader = player_party[0]
        self.player_party = player_party
        self.player_party_health = int("".join([i.health for i in self.player_party]))
        self.enemy_party = enemy_party
        self.enemy_party_health = int("".join([i.health for i in self.enemy_party]))
        self.battle_background = background
        self.bgm = bgm
        self.turn = 0

    def fight(self):
        done = False
        while not done:
            if self.turn % 2 == 0:
                # player turn
                for member in self.player_party:
                    if member.alive == False:
                        continue
                    self.battle_menu(member)
                    if enemy_party_health <= 0:
                        self.end()
            else:
                # enemy turn
                for enemy in self.enemy_party:
                    if enemy.alive == False:
                        continue
                    if all([i.alive for i in self.enemy_party]) == False:
                        self.end()


    def battle_menu(self, character):
        pass

    def end(self):
        pass

###########################################################################

############################## MAP CLASSES ################################

class TileCache(object):
    """Loads the tilesets lazily into the global cache"""

    def __init__(self, width=32, height=None):
        self.width = width
        self.height = height
        self.cache = {}

    def __getitem__(self, filename):
        key = (filename, self.width, self.height)
        try:
            return self.cache[key]
        except KeyError:
            tile_table = self._load_tile_table(filename, self.width, self.height)
            self.cche[key] = tile_table
            return tile_table

if __name__ == "__main__":
    n = Teacher("Delta", "snail_swag") # This is for testing. :p
    while True:
        n.levelup()
        print(n.level)
        inr = int(input("Value: "))
        n+inr
