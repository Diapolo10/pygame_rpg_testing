# Colours
WHITE = (255,255,255)
BLACK = (0,0,0)
GREY_DARK = (40,40,40)
GREY_LIGHT = (100,100,100)
BLUE = (0,0,255)
GREEN = (0,255,0)
GREEN_DARK = (0,170,0)
BROWN_DARK = (127,51,0)
RED = (255,0,0)
YELLOW = (255,255,0)

# Game settings
WIDTH = 640
HEIGHT = 480
FPS = 60
TITLE = "The Chronicles of Abitti"
BGCOLOUR = BROWN_DARK
TILESIZE = 32 # Changes the texture/object size, must be 16, 32 or 64
GRIDWIDTH = WIDTH / HEIGHT
GRIDHEIGHT = HEIGHT / TILESIZE

# Game sprites ##TODO: Come up with a better filename storage method
WALL_SPRITE = 'wall{0}_*.png'.format(TILESIZE) # Wall sprites

# Player settings
PLAYER_SPEED = 200
PLAYER_SPRITE = 'ilkka{0}_*.png'.format(TILESIZE) # Overworld sprites

# Enemy settings
ENEMY_SPRITE = 'sensor{0}_*.png'.format(TILESIZE) # Enemy sprites
