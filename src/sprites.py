import pygame
from config import *
vec = pygame.math.Vector2

class Player(pygame.sprite.Sprite):
    """ The player character """
    def __init__(self, game, x, y):
        """ Create the player character """
        self.groups = game.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = game.player_img[0]
        #self.image.fill(YELLOW)
        self.rect = self.image.get_rect()
        self.velocity = vec(0,0)
        self.pos = vec(x,y) * TILESIZE

    def get_keys(self):
        """ Get player input for movement and actions """
        self.velocity = vec(0,0)
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] or keys[pygame.K_a]:
            self.velocity.x = -PLAYER_SPEED
            self.image = self.game.player_img[1]
        if keys[pygame.K_RIGHT] or keys[pygame.K_d]:
            self.velocity.x = PLAYER_SPEED
            self.image = self.game.player_img[0]
        if keys[pygame.K_UP] or keys[pygame.K_w]:
            self.velocity.y = -PLAYER_SPEED
            self.image = self.game.player_img[3]
        if keys[pygame.K_DOWN] or keys[pygame.K_s]:
            self.velocity.y = PLAYER_SPEED
            self.image = self.game.player_img[2]
        if self.velocity.x != 0 and self.velocity.y != 0:
            self.velocity *= 0.7071

    def collide_with_walls(self, direction):
        """ Check for collision with any wall objects """
        if direction == 'x':
            hits = pygame.sprite.spritecollide(self, self.game.walls, False)
            if hits:
                if self.velocity.x > 0:
                    self.pos.x = hits[0].rect.left - self.rect.width
                if self.velocity.x < 0:
                    self.pos.x = hits[0].rect.right
                self.velocity.x = 0
                self.rect.x = self.pos.x
        if direction == 'y':
            hits = pygame.sprite.spritecollide(self, self.game.walls, False)
            if hits:
                if self.velocity.y > 0:
                    self.pos.y = hits[0].rect.top - self.rect.height
                if self.velocity.y < 0:
                    self.pos.y = hits[0].rect.bottom
                self.velocity.y = 0
                self.rect.y = self.pos.y

    def update(self):
        """ Update the player data, such as position """
        self.get_keys()
        self.pos += self.velocity * self.game.dt
        self.rect.x = self.pos.x
        self.collide_with_walls('x')
        self.rect.y = self.pos.y
        self.collide_with_walls('y')

class Enemy(pygame.sprite.Sprite):
    """ An enemy object """
    def __init__(self, game, x, y):
        """ Create an enemy """
        self.groups = game.all_sprites, game.enemies
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.image = game.enemy_img[0]
        self.game = game
        self.rect = self.image.get_rect()
        #self.pos = vec(x,y) * TILESIZE
        self.x = x
        self.y = y
        self.rect.x = x * TILESIZE
        self.rect.y = y * TILESIZE

class Wall(pygame.sprite.Sprite):
    """ A simple wall object """
    def __init__(self, game, x, y):
        """ Create a wall """
        self.groups = game.all_sprites, game.walls
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.theme = game.texture_theme
        self.image = game.wall_img[0]
        #pygame.Surface((TILESIZE, TILESIZE))
        #self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        self.rect.x = x * TILESIZE
        self.rect.y = y * TILESIZE
