def xml_tests():
    from xml.etree import cElementTree as et

    tree = et.parse('data.xml')
    root = tree.getroot()

    for i in root[0]:
        print(i.tag) # Prints the map names

    for i in root[0][0][0]:
        print(i.get('id')) # Prints the enemy IDs from map 0

    for i in root[0][0][0].findall('sensor'):
        print("Level {} {}".format(i.get('level'), i.get('name')))

    for i in root[0][1][0].findall('cat'):
        print(i.get('name')) # Prints enemies from map 1

    for i in root[1]:
        print(i.get('name')) # Prints all item names

    for i in root[1]:
        print(i.get('msg'))

    print(root[0][1][1][0].get('name'))

    for i in root[0]:
        if i.get('id') == "1":
            print(i[0][0].get('name'))

    for i in root[0][3][1]:
        print('{}: {}'.format(i.get('name'), i.get('target_map')))

    print('k')




if __name__ == '__main__':
    xml_tests()
