import pygame
from os.path import join
from config import *
from xml.etree import cElementTree as et

class Map(object):
    def __init__(self, filename):
        self.data = []
        with open(filename) as f:
            for line in f:
                self.data.append(line.strip())

        self.tilewidth = len(self.data[0])
        self.tileheight = len(self.data)
        self.width = self.tilewidth * TILESIZE
        self.height = self.tileheight * TILESIZE

class NewMap(object):
    def __init__(self, ID):
        self.data = []
        self.root = et.parse('data.xml').getroot()
        self.entities = [i for i in list(self.root[0][ID][0].getchildren())]
        self.exits = [i for i in list(self.root[0][ID][1].getchildren())]
        try:
            self.theme = self.root[0][ID][2].get('folder')
        except IndexError:
            self.theme = ''

        with open(join('maps', 'map{}.dat'.format(ID))) as f:
            for line in f:
                self.data.append(line.strip())

        self.tilewidth = len(self.data[0])
        self.tileheight = len(self.data)
        self.width = self.tilewidth * TILESIZE
        self.height = self.tileheight * TILESIZE

class Camera(object):
    def __init__(self, width, height):
        self.camera = pygame.Rect(0, 0, width, height)
        self.width = width
        self.height = height

    def apply(self, entity):
        return entity.rect.move(self.camera.topleft)

    def update(self, target):
        x = -target.rect.x + int(WIDTH / 2)
        y = -target.rect.y + int(HEIGHT / 2)

        # Limit scrolling to map size
        x = min(0, x) # Left
        y = min(0, y) # Top
        x = max(-(self.width - WIDTH), x) # Right
        y = max(-(self.height - HEIGHT), y) # Bottom
        self.camera = pygame.Rect(x, y, self.width, self.height)
