# Pygame RPG

#### For more detailed documentation, please read [the main documentation](./docs/main_documentation.md "The official main documentation").

#### You may read the licence agreement [here](./LICENSE "The licence agreement").

#### Current design plans can be found [here](./docs/design.md "Design plans").
